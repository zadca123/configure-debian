.DEFAULT_GOAL := help
.PHONY: help run install clean list-tags select-tag
ANSIBLE_OPTIONS =
PLAYBOOK = ./debian.yml
REQUIREMENTS = pyproject.toml
TAG = $(filter-out $@, $(MAKECMDGOALS)) # := not works!

# run all stuff
all:
	poetry run ansible-playbook $(PLAYBOOK) -e ansible_user="$$USER" $(ANSIBLE_OPTIONS)

# run specified tags, just pass it after this keyword, comma separated
tags:
	poetry run ansible-playbook $(PLAYBOOK) -e ansible_user="$$USER" --tags=$(TAG)

# list tags of the $(PLAYBOOK) variable
list:
	@poetry run ansible-playbook $(PLAYBOOK) --list-tags | grep "TASK TAGS" | sed -n 's~.*TASK TAGS: \[\(.*\)\]~\1~p' | tr ',' '\n'

# install dependencies
install: $(REQUIREMENTS)
	poetry install

# generate requirements.txt file
requirements:
	@poetry export --format requirements.txt --output requirements.txt

# Show this help.
help:
	@awk '/^#/{c=substr($$0,3);next}c&&/^[[:alpha:]][[:alnum:]_-]+:/{print substr($$1,1,index($$1,":")),c}1{c=0}' $(MAKEFILE_LIST) | column -s: -t
	@echo "\nUsage:"
	@echo "  make list-tags"
	@echo "  make select-tag setup_cron"
	@echo "  make run ANSIBLE_OPTIONS='--extra-vars \"key=value\"'"
	@echo "  make run ANSIBLE_OPTIONS='--start-at-task \"task_name\"'"
